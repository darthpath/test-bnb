﻿using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace BNB.Models
{
    public class PRODUTO_Model
    {
        public string COD_PRODUTO { get; set; }
        public string DES_PRODUTO { get; set; }
        /// <summary>
        /// Status do Registro
        /// A = Ativo
        /// I = Inativo
        /// </summary>
        public char STA_STATUS { get; set; }

        public List<PRODUTO_Model> GetAllProdutos()
        {
            var p = new DB.ProdutosController();
            var listP = p.GetProdutos();
            List<PRODUTO_Model> pm = new List<PRODUTO_Model>();
            foreach(var i in listP)
            {
                pm.Add(new PRODUTO_Model()
                {
                    COD_PRODUTO = i.COD_PRODUTO
                    , DES_PRODUTO = i.DES_PRODUTO
                    , STA_STATUS = Convert.ToChar(i.STA_STAUS)
                });
            }
            return pm;
        }
    }
}