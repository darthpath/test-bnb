﻿using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BNB.Models
{
    public class MOVIMENTO_MANUAL_Model: MovimentoController
    {
        public int DAT_MES { get; set; }
        public int DAT_ANO { get; set; }
        public int NUM_LANCAMENTO { get; set; }
        public string COD_PRODUTO { get; set; }
        public string COD_COSIF { get; set; }
        public decimal VAL_VALOR { get; set; }
        public string DES_DESCRICAO { get; set; }
        public DateTime DAT_MOVIMENTO { get; set; }
        public string COD_USUARIO { get; set; }

        public string DES_PRODUTO { get; set; }
        public List<MOVIMENTO_MANUAL_Model> GetMovimentos()
        {
            var movimentos = GetAllMovimentosComDescricao();
            List<MOVIMENTO_MANUAL_Model> l = new List<MOVIMENTO_MANUAL_Model>();
            foreach(var m in movimentos)
            {
                l.Add(new MOVIMENTO_MANUAL_Model()
                {
                    COD_COSIF = m.COD_COSIF
                    ,
                    COD_PRODUTO = m.COD_PRODUTO
                    ,
                    COD_USUARIO = m.COD_USUARIO
                    ,
                    DAT_ANO = (int)m.DAT_ANO
                    ,
                    DAT_MES = (int)m.DAT_MES
                    ,
                    DAT_MOVIMENTO = m.DAT_MOVIMENTO
                    ,
                    DES_DESCRICAO = m.DES_DESCRICAO
                    ,
                    VAL_VALOR = m.VAL_VALOR
                    ,
                    DES_PRODUTO = m.DES_PRODUTO
                    
                    ,NUM_LANCAMENTO = (int)m.NUM_LANCAMENTO
                });
            }
            return l;
        }
        public bool AddMovimentoManual(MOVIMENTO_MANUAL_Model m)
        {
            try
            {
                var movimento_ = new MOVIMENTO_MANUAL()
                {
                    COD_COSIF = m.COD_COSIF
                    ,
                    COD_PRODUTO = m.COD_PRODUTO
                    ,
                    COD_USUARIO = m.COD_USUARIO
                    ,
                    DAT_ANO = m.DAT_ANO
                    ,
                    DAT_MES = m.DAT_MES
                    ,
                    DAT_MOVIMENTO = DateTime.Now
                    ,
                    DES_DESCRICAO = m.DES_DESCRICAO
                    ,
                    VAL_VALOR = m.VAL_VALOR
                };
                AddMovimento(movimento_);
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }
    }
}