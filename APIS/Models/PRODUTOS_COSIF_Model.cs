﻿using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNB.Models
{
    public class PRODUTOS_COSIF_Model
    {
        public string COD_PRODUTO { get; set; }
       
        public string COD_COSIF { get; set; }
        /// <summary>
        /// COSIF: Normal ou MTM
        /// </summary>
        public string COD_CLASSIFICACAO { get; set; }
        /// <summary>
        /// Status do Registro
        /// A = Ativo
        /// I = Inativo
        /// </summary>
        public char STA_STATUS { get; set; }

        public List<PRODUTOS_COSIF_Model> GetAllProdutosCosIf()
        {
            var p = new DB.ProdutosController();
            var listP = p.GetPRODUTO_COSIFs();
            List<PRODUTOS_COSIF_Model> pm = new List<PRODUTOS_COSIF_Model>();
            foreach (var i in listP)
            {
                pm.Add(new PRODUTOS_COSIF_Model()
                {
                    COD_PRODUTO = i.COD_PRODUTO
                    ,COD_CLASSIFICACAO = i.COD_CLASSIFICACAO
                    ,COD_COSIF = i.COD_COSIF
                    ,STA_STATUS = Convert.ToChar(i.STA_STATU)
                });
            }
            return pm;
        }
    }
}