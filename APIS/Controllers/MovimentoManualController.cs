﻿using BNB.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace BNB.APIS
{
    public class MovimentoManualController : ApiController
    {
        // GET api/<controller>
        private List<MOVIMENTO_MANUAL_Model> movimentos = new List<MOVIMENTO_MANUAL_Model>();

        public MovimentoManualController() { }
        public MovimentoManualController(List<MOVIMENTO_MANUAL_Model> movimentos)
        {
            this.movimentos = movimentos;
        }
        //public IEnumerable<MOVIMENTO_MANUAL_Model> _Movimentos()
        //{
        //    return new MOVIMENTO_MANUAL_Model().GetMovimentos();
        //}
        [HttpGet]
        public async Task<IEnumerable<MOVIMENTO_MANUAL_Model>> GetAllMovimentosAsync()
        {
            return await Task.FromResult(new MOVIMENTO_MANUAL_Model().GetMovimentos());
        }
        //public async Task<IHttpActionResult> MovimentoAsync(int id)
        //{
        //    return await Task.FromResult(GetMovimento(id));
        //}
        //public IHttpActionResult GetMovimento(int id)
        //{
        //    var moviment = movimentos.FirstOrDefault((p) => p.NUM_LANCAMENTO == id);
        //    if (moviment == null)
        //    {
        //        return NotFound();
        //    }
        //    return Movimento(moviment);
        //}
        //private OkNegotiatedContentResult<MOVIMENTO_MANUAL_Model> Movimento(MOVIMENTO_MANUAL_Model movimento)
        //{
        //    return Ok(movimento);
        //}
        [HttpPost]
        public IHttpActionResult PostMovimento(MOVIMENTO_MANUAL_Model movimento)
        {
            var m = new MOVIMENTO_MANUAL_Model();
            if (m.AddMovimentoManual(movimento))
            {
                return Ok(movimento);
            }
            else
            {
                return null;
            }
        }
    }
}