﻿using BNB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace BNB
{
    public class ProdutosController : ApiController
    {
        // GET api/<controller>

        private List<PRODUTO_Model> produtos = new List<PRODUTO_Model>();
        public ProdutosController() { }

        public ProdutosController(List<PRODUTO_Model> produtos)
        {
            this.produtos = produtos;
        }
        public IEnumerable<PRODUTO_Model> AllProdutos()
        {
            return new PRODUTO_Model().GetAllProdutos();
        }
        [HttpGet]
        public async Task<IEnumerable<PRODUTO_Model>> GetAllProdutosAsync()
        {
            return await Task.FromResult(AllProdutos());
        }
        private OkNegotiatedContentResult<PRODUTO_Model> Produto(PRODUTO_Model product)
        {
            return Ok(product);
        }

        //public IHttpActionResult Produto(string id)
        //{
        //    var product = produtos.FirstOrDefault((p) => p.COD_PRODUTO == id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }
        //    return Produto(product);
        //}

        //public async Task<IHttpActionResult> GetProductosAsync(string id)
        //{
        //    return await Task.FromResult(Produto(id));
        //}

        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}