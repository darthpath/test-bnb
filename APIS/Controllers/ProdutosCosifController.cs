﻿using BNB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace BNB.APIS
{
    public class ProdutosCosifController : ApiController
    {
        private List<PRODUTOS_COSIF_Model> prodsCosif = new List<PRODUTOS_COSIF_Model>();
        public ProdutosCosifController() { }

        public ProdutosCosifController(List<PRODUTOS_COSIF_Model> prodsCosif) 
        {
            this.prodsCosif = new PRODUTOS_COSIF_Model().GetAllProdutosCosIf();
        }
        // GET api/<controller>
        public IEnumerable<PRODUTOS_COSIF_Model> AllProdutosCosif()
        {
            return new PRODUTOS_COSIF_Model().GetAllProdutosCosIf();
        }
        [HttpGet]
        public async Task<IEnumerable<PRODUTOS_COSIF_Model>> GetAllProdutosCosifAsync()
        {
            return await Task.FromResult(AllProdutosCosif());
        }


        //public IHttpActionResult ProdutoCosif(string id)
        //{
        //    var product = prodsCosif.FirstOrDefault((p) => p.COD_PRODUTO == id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }
        //    return ProdCosIf(product);
        //}

        //private OkNegotiatedContentResult<PRODUTOS_COSIF_Model> ProdCosIf(PRODUTOS_COSIF_Model product)
        //{
        //    return Ok(product);
        //}
    }
}