﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BNB.Models;
using System.Web.Http;
using System.Web.Http.Results;
using BNB.APIS;

namespace BNB.Tests
{
    /// <summary>
    /// Summary description for ProdutoTest
    /// </summary>
    [TestClass]
    public class ProdutoCosIfTest
    {
        [TestMethod]
        public void GetAllProdutos()
        {
            var testProducts = GetTestProducts();
            var controller = new ProdutosCosifController(testProducts);

            var result = controller.AllProdutosCosif() as List<PRODUTOS_COSIF_Model>;
            Assert.AreEqual(testProducts.Count, result.Count);
        }

        //[TestMethod]
        //public void GetProdutoByCod_Prod()
        //{
        //    var testProducts = GetTestProducts();
        //    var controller = new ProdutosCosifController(testProducts);

        //    IHttpActionResult actionResult = controller.ProdutoCosif("0");
        //    var result = actionResult as OkNegotiatedContentResult<PRODUTOS_COSIF_Model>;

        //    Assert.IsNotNull(result);
        //    Assert.IsNotNull(result.Content);
        //    Assert.AreEqual(testProducts[0].COD_PRODUTO, result.Content.COD_PRODUTO);
        //}

        private List<PRODUTOS_COSIF_Model> GetTestProducts()
        {
            var testProducts = new List<PRODUTOS_COSIF_Model>
            {
                new PRODUTOS_COSIF_Model { COD_PRODUTO = "0", COD_COSIF = "00000000000", COD_CLASSIFICACAO = "NORMAL", STA_STATUS = 'A' },
                new PRODUTOS_COSIF_Model { COD_PRODUTO = "1", COD_COSIF = "00000000000", COD_CLASSIFICACAO = "NORMAL", STA_STATUS = 'A' },
                new PRODUTOS_COSIF_Model { COD_PRODUTO = "2", COD_COSIF = "00000000000", COD_CLASSIFICACAO = "MTM", STA_STATUS = 'I' },
                new PRODUTOS_COSIF_Model { COD_PRODUTO = "3", COD_COSIF = "00000000000", COD_CLASSIFICACAO = "NORMAL", STA_STATUS = 'A' },
                new PRODUTOS_COSIF_Model { COD_PRODUTO = "4", COD_COSIF = "00000000000", COD_CLASSIFICACAO = "MTM", STA_STATUS = 'A' },
                new PRODUTOS_COSIF_Model { COD_PRODUTO = "5", COD_COSIF = "00000000000", COD_CLASSIFICACAO = "NORMAL", STA_STATUS = 'A' },
                new PRODUTOS_COSIF_Model { COD_PRODUTO = "6", COD_COSIF = "00000000000", COD_CLASSIFICACAO = "NORMAL", STA_STATUS = 'I' },
                new PRODUTOS_COSIF_Model { COD_PRODUTO = "7", COD_COSIF = "00000000000", COD_CLASSIFICACAO = "MTM", STA_STATUS = 'A' }
            };
            return testProducts;
        }
    }
}
