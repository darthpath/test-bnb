﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DB;
using System.Security.Cryptography;

namespace BNB.Tests
{
    /// <summary>
    /// Summary description for DBSTest
    /// </summary>
    [TestClass]
    public class DBSTest
    {
        [TestMethod]
        public void AddProdutos()
        {
            PRODUTO p = new PRODUTO()
            {
                COD_PRODUTO = "0"
                , DES_PRODUTO = "Teste 0"
                , STA_STAUS = "A"
            };
            DB.ProdutosController pdb = new DB.ProdutosController();
            pdb.AddProduto(p);
        }

        [TestMethod]
        public void AddProdutosCosIf()
        {
            PRODUTO_COSIF p = new PRODUTO_COSIF()
            {
                COD_PRODUTO = "0"
                , STA_STATU = "A"
                , COD_CLASSIFICACAO = "NORMAL"
                , COD_COSIF = "1"
            };
            DB.ProdutosController pdb = new DB.ProdutosController();
            pdb.AddProdutoCosIf(p);
        }

        [TestMethod]
        public void GetProdutos()
        {
            DB.ProdutosController pdb = new DB.ProdutosController();
            Assert.IsTrue(pdb.GetProdutos().Count >= 1);
        }

        [TestMethod]
        public void GetProdutosCoIf()
        {
            DB.ProdutosController pdb = new DB.ProdutosController();
            Assert.IsTrue(pdb.GetPRODUTO_COSIFs().Count >= 1);
        }

        [TestMethod]
        public void AddMovimentoManual()
        {
            MovimentoController mv = new MovimentoController();
            foreach(var v in GetTestMovimentos())
            {
                mv.AddMovimento(v);
            }
        }

        [TestMethod]
        public void GetAllMovimentoManual()
        {
            MovimentoController mv = new MovimentoController();
            Assert.IsTrue(mv.GetAllMovimentos().Count >= 1);
        }


        private List<MOVIMENTO_MANUAL> GetTestMovimentos()
        {
            var testMovimentos = new List<MOVIMENTO_MANUAL>
            {
                new MOVIMENTO_MANUAL {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 8, COD_PRODUTO = "0", COD_COSIF="0", VAL_VALOR= (decimal)1.00, DES_DESCRICAO = "Produto 0", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "TESTE" },
                new MOVIMENTO_MANUAL {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 1, COD_PRODUTO = "1", COD_COSIF="0", VAL_VALOR= (decimal)1.50, DES_DESCRICAO = "Produto 1", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "TESTE" },
                new MOVIMENTO_MANUAL {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 2, COD_PRODUTO = "2", COD_COSIF="0", VAL_VALOR= (decimal)1.97, DES_DESCRICAO = "Produto 2", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "TESTE" },
                new MOVIMENTO_MANUAL {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 9, COD_PRODUTO = "3", COD_COSIF="0", VAL_VALOR= (decimal)1.00, DES_DESCRICAO = "Produto 3", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "TESTE" },
                new MOVIMENTO_MANUAL {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 4, COD_PRODUTO = "4", COD_COSIF="0", VAL_VALOR= (decimal)1.88, DES_DESCRICAO = "Produto 4", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "TESTE" },
                new MOVIMENTO_MANUAL {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 5, COD_PRODUTO = "5", COD_COSIF="0", VAL_VALOR= (decimal)0.20, DES_DESCRICAO = "Produto 5", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "TESTE" },
                new MOVIMENTO_MANUAL {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 6, COD_PRODUTO = "6", COD_COSIF="0", VAL_VALOR= (decimal)0.99, DES_DESCRICAO = "Produto 6", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "TESTE" },
                new MOVIMENTO_MANUAL {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 7, COD_PRODUTO = "7", COD_COSIF="0", VAL_VALOR= (decimal)1.99, DES_DESCRICAO = "Produto 7", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "TESTE" }
            };
            return testMovimentos;
        }
    }
}
