﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using System.Web.Http.Results;
using BNB.Models;
using BNB.APIS;
using System.Linq;
using System;

namespace BNB.Tests
{
    [TestClass]
    public class MovimentoManualTest
    {
        [TestMethod]
        public void GetAllMovimentoManual()
        {
            var movimentos = GetTestMovimentos();
            var controller = new MovimentoManualController(movimentos);

            var result = controller._Movimentos();
            Assert.AreEqual(movimentos.Count, result.Count());
        }

        //[TestMethod]
        //public void GetMovimentoManualByNumLanc()
        //{
        //    var movimentos = GetTestMovimentos();
        //    var controller = new MovimentoManualController(movimentos);

        //    IHttpActionResult actionResult = controller.GetMovimento(0);
        //    var result = actionResult as OkNegotiatedContentResult<MOVIMENTO_MANUAL_Model>;

        //    Assert.IsNotNull(result);
        //    Assert.IsNotNull(result.Content);
        //    Assert.AreEqual(movimentos[0].NUM_LANCAMENTO, result.Content.NUM_LANCAMENTO);
        //}
        private List<MOVIMENTO_MANUAL_Model> GetTestMovimentos()
        {
            var testMovimentos = new List<MOVIMENTO_MANUAL_Model>
            {
                new MOVIMENTO_MANUAL_Model {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 0, COD_PRODUTO = "0", COD_COSIF="0", VAL_VALOR= (decimal)1.00, DES_DESCRICAO = "Produto 0", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "123456789A" },
                new MOVIMENTO_MANUAL_Model {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 0, COD_PRODUTO = "1", COD_COSIF="0", VAL_VALOR= (decimal)1.50, DES_DESCRICAO = "Produto 1", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "123456789B" },
                new MOVIMENTO_MANUAL_Model {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 0, COD_PRODUTO = "2", COD_COSIF="0", VAL_VALOR= (decimal)1.97, DES_DESCRICAO = "Produto 2", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "123456789C" },
                new MOVIMENTO_MANUAL_Model {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 0, COD_PRODUTO = "3", COD_COSIF="0", VAL_VALOR= (decimal)1.00, DES_DESCRICAO = "Produto 3", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "123456789D" },
                new MOVIMENTO_MANUAL_Model {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 0, COD_PRODUTO = "4", COD_COSIF="0", VAL_VALOR= (decimal)1.88, DES_DESCRICAO = "Produto 4", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "123456789E" },
                new MOVIMENTO_MANUAL_Model {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 0, COD_PRODUTO = "5", COD_COSIF="0", VAL_VALOR= (decimal)0.20, DES_DESCRICAO = "Produto 5", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "123456789F" },
                new MOVIMENTO_MANUAL_Model {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 0, COD_PRODUTO = "6", COD_COSIF="0", VAL_VALOR= (decimal)0.99, DES_DESCRICAO = "Produto 6", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "123456789G" },
                new MOVIMENTO_MANUAL_Model {DAT_MES = 1, DAT_ANO = 2019, NUM_LANCAMENTO = 0, COD_PRODUTO = "7", COD_COSIF="0", VAL_VALOR= (decimal)1.99, DES_DESCRICAO = "Produto 7", DAT_MOVIMENTO = DateTime.Now, COD_USUARIO = "123456789H" }
            };
            return testMovimentos;
        }

    }
}
