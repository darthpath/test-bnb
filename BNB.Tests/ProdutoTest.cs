﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using System.Web.Http.Results;
using APIS;
using BNB.Models;

namespace BNB.Tests
{
    /// <summary>
    /// Summary description for ProdutoTest
    /// </summary>
    [TestClass]
    public class ProdutoTest
    {
        [TestMethod]
        public void GetAllProdutos()
        {
            var testProducts = GetTestProducts();
            var controller = new ProdutosController(testProducts);

            var result = controller.AllProdutos() as List<PRODUTO_Model>;
            Assert.AreEqual(testProducts.Count, result.Count);
        }

        //[TestMethod]
        //public void GetProdutoByCod_Prod()
        //{
        //    var testProducts = GetTestProducts();
        //    var controller = new ProdutosController(testProducts);

        //    IHttpActionResult actionResult = controller.GetProduto("0");
        //    var result = actionResult as OkNegotiatedContentResult<PRODUTO_Model>;

        //    Assert.IsNotNull(result);
        //    Assert.IsNotNull(result.Content);
        //    Assert.AreEqual(testProducts[0].COD_PRODUTO, result.Content.COD_PRODUTO);
        //}

        private List<PRODUTO_Model> GetTestProducts()
        {
            var testProducts = new List<PRODUTO_Model>
            {
                new PRODUTO_Model { COD_PRODUTO = "0", DES_PRODUTO = "Produto 0", STA_STATUS = 'A' },
                new PRODUTO_Model { COD_PRODUTO = "1", DES_PRODUTO = "Produto 1", STA_STATUS = 'A' },
                new PRODUTO_Model { COD_PRODUTO = "2", DES_PRODUTO = "Produto 2", STA_STATUS = 'I' },
                new PRODUTO_Model { COD_PRODUTO = "3", DES_PRODUTO = "Produto 3", STA_STATUS = 'A' },
                new PRODUTO_Model { COD_PRODUTO = "4", DES_PRODUTO = "Produto 4", STA_STATUS = 'A' },
                new PRODUTO_Model { COD_PRODUTO = "5", DES_PRODUTO = "Produto 5", STA_STATUS = 'A' },
                new PRODUTO_Model { COD_PRODUTO = "6", DES_PRODUTO = "Produto 6", STA_STATUS = 'I' },
                new PRODUTO_Model { COD_PRODUTO = "7", DES_PRODUTO = "Produto 7", STA_STATUS = 'A' }
            };
            return testProducts;
        }
    }
}
