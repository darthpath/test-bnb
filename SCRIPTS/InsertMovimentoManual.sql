SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Insert new Movismentos Manuais
-- =============================================
CREATE PROCEDURE InsertMovimentoManual 
@DAT_MES			numeric(18,0)
,@DAT_ANO			numeric(18,0)
,@COD_PRODUTO		char(4)
,@COD_COSIF			char(11)
,@VAL_VALOR			numeric(18,2)
,@DES_DESCRICAO		varchar(50)
,@COD_USUARIO		varchar(15)
AS
BEGIN

	SET NOCOUNT ON;
	INSERT INTO [dbo].[MOVIMENTO_MANUAL]
           ([DAT_MES]
           ,[DAT_ANO]
           ,[NUM_LANCAMENTO]
           ,[COD_PRODUTO]
           ,[COD_COSIF]
           ,[VAL_VALOR]
           ,[DES_DESCRICAO]
           ,[DAT_MOVIMENTO]
           ,[COD_USUARIO])
     VALUES
           (@DAT_MES
           ,@DAT_ANO
           ,(SELECT NEXT VALUE FOR NUM_LANCAMENTO)
           ,@COD_PRODUTO
           ,@COD_COSIF
           ,@VAL_VALOR
           ,@DES_DESCRICAO
           ,GETDATE()
           ,@COD_USUARIO)

END
GO
