SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Get all Produtos COSIF table data
-- =============================================
CREATE PROCEDURE GetProdutosCosIf 
AS
BEGIN

	SET NOCOUNT ON;

/****** Script for SelectTopNRows command from SSMS  ******/
	SELECT [COD_PRODUTO]
      ,[COD_COSIF]
      ,[COD_CLASSIFICACAO]
      ,[STA_STATU]
  FROM [BNBTeste].[dbo].[PRODUTO_COSIF]
END
GO
