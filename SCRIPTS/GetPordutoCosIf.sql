USE [BNBTeste]
GO

/****** Object:  StoredProcedure [dbo].[GetProdutosCosIf]    Script Date: 5/13/2020 3:15:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Get a Produto COSIF table data
-- =============================================
CREATE PROCEDURE [dbo].[GetProdutoCosIf] 
@COD_PRODUTO CHAR(4)
,@COD_COSIF  CHAR(11)
AS
BEGIN

	SET NOCOUNT ON;

/****** Script for SelectTopNRows command from SSMS  ******/
	SELECT [COD_PRODUTO]
      ,[COD_COSIF]
      ,[COD_CLASSIFICACAO]
      ,[STA_STATU]
  FROM [BNBTeste].[dbo].[PRODUTO_COSIF]
  WHERE [COD_PRODUTO] = @COD_PRODUTO
  AND [COD_COSIF] = @COD_COSIF
END
GO


