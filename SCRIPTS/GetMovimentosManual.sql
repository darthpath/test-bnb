USE [BNBTeste]
GO

/****** Object:  StoredProcedure [dbo].[GetAllMovimentoManual]    Script Date: 5/13/2020 3:17:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Get All Movimentações Manuais
-- =============================================
CREATE PROCEDURE [dbo].[GetMovimentoManual]
@DAT_MES			numeric(18)		NULL
,@DAT_ANO			numeric(18)		NULL
,@NUM_LANCAMENTO	numeric(18)		NULL
,@COD_PRODUTO		char(4)			NULL
,@COD_COSIF			char(11)		NULL
,@DAT_MOVIMENTO		smalldatetime	NULL
,@COD_USUARIO		varchar(15)		NULL
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
	   [DAT_MES]
      ,[DAT_ANO]
      ,[NUM_LANCAMENTO]
      ,[COD_PRODUTO]
      ,[COD_COSIF]
      ,[VAL_VALOR]
      ,[DES_DESCRICAO]
      ,[DAT_MOVIMENTO]
      ,[COD_USUARIO]
  FROM [BNBTeste].[dbo].[MOVIMENTO_MANUAL]
  WHERE  (@DAT_MES			IS NOT NULL OR [DAT_MES]			=	@DAT_MES		)
	AND	 (@DAT_ANO			IS NOT NULL	OR [DAT_ANO]			=	@DAT_ANO		)
 	AND	 (@NUM_LANCAMENTO	IS NOT NULL	OR [NUM_LANCAMENTO]		=	@NUM_LANCAMENTO )
	AND	 (@COD_PRODUTO		IS NOT NULL	OR [COD_PRODUTO]		=	@COD_PRODUTO	)
	AND	 (@COD_COSIF		IS NOT NULL	OR [COD_COSIF]			=	@COD_COSIF		)
	AND	 (@DAT_MOVIMENTO	IS NOT NULL	OR [DAT_MOVIMENTO]		=	@DAT_MOVIMENTO	)
	AND	 (@COD_USUARIO		IS NOT NULL	OR [COD_USUARIO]		=	@COD_USUARIO	)

END	
GO	