USE [BNBTeste]
GO
/****** Object:  StoredProcedure [dbo].[InsertProdutoCosIf]    Script Date: 5/14/2020 5:41:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Insert into ProdutoCosIf
-- =============================================
CREATE PROCEDURE [dbo].[InsertProdutoCosIf]
@COD_PRODUTO			char(4)
,@COD_COSIF				varchar(11)
,@COD_CLASSIFICACAO		char(6)
,@STA_STATU				char(1)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[PRODUTO_COSIF]
           ([COD_PRODUTO]
           ,[COD_COSIF]
           ,[COD_CLASSIFICACAO]
           ,[STA_STATU])
     VALUES
           (@COD_PRODUTO
           ,@COD_COSIF
           ,@COD_CLASSIFICACAO
           ,@STA_STATU)
END
GO
