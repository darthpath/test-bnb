SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Retrieve all produtos from PRODUTOS TABLE
-- =============================================
CREATE PROCEDURE GetProdutos 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [COD_PRODUTO]
      ,[DES_PRODUTO]
      ,[STA_STAUS]
  FROM [BNBTeste].[dbo].[PRODUTO]
END
GO
