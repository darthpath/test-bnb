USE [BNBTeste]
GO
/****** Object:  StoredProcedure [dbo].[InsertProduto]    Script Date: 5/14/2020 5:41:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Insert a New Produto
-- =============================================
CREATE PROCEDURE [dbo].[InsertProduto] 
@COD_PRODUTO	CHAR(4)
,@DES_PRODUTO	VARCHAR(30)
,@STA_STATUS	CHAR(1)
AS
BEGIN
	INSERT INTO [dbo].[PRODUTO]
           ([COD_PRODUTO]
           ,[DES_PRODUTO]
           ,[STA_STAUS])
     VALUES
           (@COD_PRODUTO
           ,@DES_PRODUTO
           ,@STA_STATUS)
END
GO
