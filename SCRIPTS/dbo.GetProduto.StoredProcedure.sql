USE [BNBTeste]
GO
/****** Object:  StoredProcedure [dbo].[GetProduto]    Script Date: 5/14/2020 5:41:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Get A Specific Produto
-- =============================================
CREATE PROCEDURE [dbo].[GetProduto] 
	@COD_PRODUTO CHAR(4)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT [COD_PRODUTO]
      ,[DES_PRODUTO]
      ,[STA_STAUS]
  FROM [dbo].[PRODUTO]
  WHERE [COD_PRODUTO] = @COD_PRODUTO
END
GO
