
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Charles Path
-- Create date: 13/05/2020
-- Description:	Get All Movimentações Manuais
-- =============================================
CREATE PROCEDURE GetAllMovimentoManual
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [DAT_MES]
      ,[DAT_ANO]
      ,[NUM_LANCAMENTO]
      ,[COD_PRODUTO]
      ,[COD_COSIF]
      ,[VAL_VALOR]
      ,[DES_DESCRICAO]
      ,[DAT_MOVIMENTO]
      ,[COD_USUARIO]
  FROM [BNBTeste].[dbo].[MOVIMENTO_MANUAL]
END
GO
