﻿function GetProdutos() {
    $.ajax({
        type: "GET"
        , dataType: "json"
        , url: "/api/Produtos"
        , success: function (data) {
            //console.log(data);
            LoadProdutos(data);
        }
    });
}

function GetProdutosCosif() {
    $.ajax({
        type: "GET"
        , dataType: "json"
        , url: "/api/ProdutosCosif"
        , success: function (data) {
            //console.log(data);
            LoadProdutosCosif(data);
        }
    });
}

function LoadProdutos(data) {
    for (i = 0; i <= data.length; i++) {
        $('#produtopicker').append($('<option/>').val(data[i].COD_PRODUTO).html(data[i].DES_PRODUTO))
    }
}

function LoadProdutosCosif(data) {
    for (i = 0; i <= data.length; i++) {
        $('#produtocosifpicker').append($('<option/>').val(data[i].COD_COSIF).html(data[i].COD_COSIF))
    }
}
