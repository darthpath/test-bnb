﻿function LoadYears() {
    for (i = new Date().getFullYear(); i > 1900; i--) {
        $('#yearpicker').append($('<option />').val(i).html(i));
    }
}

function LoadMonths() {
    for (i = 12; i >= 1; i--) {
        $('#monthpicker').append($('<option />').val(i).html(i));
    }
}

