﻿function GetMovimentacoes() {
    $.ajax({
        type: "GET"
        , dataType: "json"
        , url: "/api/MovimentoManual"
        , success: function (data) {
            //console.log(data);
            UpdateGrid(data);
        }
    });
}

function UpdateGrid(data) {
    for (var i = 0; i < data.length; i++) {
        $('#movimentacoes').append('<tr><td>' + data[i].DAT_MES + '</td>'
            + '<td>' + data[i].DAT_ANO + '</td>'
            + '<td>' + data[i].COD_PRODUTO + '</td>'
            + '<td>' + data[i].DES_PRODUTO+'</td>'
            + '<td>' + data[i].NUM_LANCAMENTO + '</td>'
            + '<td>' + data[i].DES_DESCRICAO + '</td>'
            + '<td> $' + data[i].VAL_VALOR + '</td>'
            + '</tr>');
    }
}

function BuildObjetc() {
    var MOVIMENTO_MANUAL_Model = new Object();
    MOVIMENTO_MANUAL_Model.DAT_MES          = $('#monthpicker').val();
    MOVIMENTO_MANUAL_Model.DAT_ANO          = $('#yearpicker').val();$
    MOVIMENTO_MANUAL_Model.NUM_LANCAMENTO   = ""
    MOVIMENTO_MANUAL_Model.COD_PRODUTO      = $('#produtopicker').val();
    MOVIMENTO_MANUAL_Model.COD_COSIF        = $('#produtocosifpicker').val();
    MOVIMENTO_MANUAL_Model.VAL_VALOR        = $('#valor').val();
    MOVIMENTO_MANUAL_Model.DAT_MOVIMENTO    = "";
    MOVIMENTO_MANUAL_Model.DES_DESCRICAO    = $('#descricao').val()
    MOVIMENTO_MANUAL_Model.COD_USUARIO      = "";

    AddNovoMovimento(MOVIMENTO_MANUAL_Model)
};

function AddNovoMovimento(model) {
    var _model = JSON.stringify(model);

    $.ajax({
        type: "POST"
        , dataType: "json"
        , data: _model
        , contentType: 'application/json; charset=utf-8'
        , url: "/api/MovimentoManual/PostMovimento"
        , success: function (data) {
            console.log(data);
            GetMovimentacoes();
        }
    });
    
};