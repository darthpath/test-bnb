﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class MovimentoController
    {
        public void AddMovimento(MOVIMENTO_MANUAL movMan)
        {
            using (var db = new BNBEntities())
            {
                var maxId = db.MOVIMENTO_MANUAL.Max(table => table.NUM_LANCAMENTO);
                var newId = maxId + 1;
                movMan.COD_USUARIO = "TESTE";
                movMan.NUM_LANCAMENTO = newId;
                db.MOVIMENTO_MANUAL.Add(movMan);
                db.SaveChanges();
            }
        }

        public List<MOVIMENTO_MANUAL> GetAllMovimentos()
        {
            using (var db = new BNBEntities())
            {
                var query = from mv in db.MOVIMENTO_MANUAL orderby mv.DAT_ANO orderby mv.DAT_MES select mv;
                return query.ToList();
            }
        }

        public List<ListaMovimentoManual> GetAllMovimentosComDescricao()
        {
            using (var db = new BNBEntities())
            {
                var query = from mv in db.MOVIMENTO_MANUAL orderby mv.DAT_ANO orderby mv.DAT_MES select mv;

                var query2 = from mv in db.MOVIMENTO_MANUAL
                             join prd in db.PRODUTOes on new { mv.COD_PRODUTO }
                                equals new { prd.COD_PRODUTO }
                             select new
                             {
                                 COD_COSIF = mv.COD_COSIF
                             ,
                                 COD_PRODUTO = mv.COD_PRODUTO
                             ,
                                 COD_USUARIO = mv.COD_PRODUTO
                             ,
                                 DAT_ANO = mv.DAT_ANO
                             ,
                                 DAT_MES = mv.DAT_MES
                             ,
                                 DAT_MOVIMENTO = mv.DAT_MOVIMENTO
                             ,
                                 DES_DESCRICAO = mv.DES_DESCRICAO
                             ,
                                 DES_PRODUTO = prd.DES_PRODUTO
                                 ,
                                 VAL_VALOR = mv.VAL_VALOR
                                ,
                                 NUM_LANCAMENTO = mv.NUM_LANCAMENTO
                             };
                List<ListaMovimentoManual> l = new List<ListaMovimentoManual>();
                foreach(var item in query2)
                {
                    l.Add(new ListaMovimentoManual()
                    {
                        DAT_MES = item.DAT_MES
                        ,
                        DAT_ANO = item.DAT_ANO
                        ,
                        COD_COSIF = item.COD_COSIF
                        ,
                        COD_PRODUTO = item.COD_PRODUTO
                        ,
                        COD_USUARIO = item.COD_USUARIO
                        ,
                        DAT_MOVIMENTO = item.DAT_MOVIMENTO
                        ,
                        DES_DESCRICAO = item.DES_DESCRICAO
                        ,
                        VAL_VALOR = item.VAL_VALOR
                        ,
                        DES_PRODUTO = item.DES_PRODUTO
                        ,
                        NUM_LANCAMENTO = item.NUM_LANCAMENTO
                    });
                }

                return l;
            }
        }
    }
}
