﻿using System.Collections.Generic;
using System.Linq;

namespace DB
{
    public class ProdutosController
    {
        public List<PRODUTO> GetProdutos()
        {
            using (var db = new BNBEntities())
            {
                var query = from b in db.PRODUTOes orderby b.COD_PRODUTO select b;
                return query.ToList();
            }

        }

        public List<PRODUTO_COSIF> GetPRODUTO_COSIFs()
        {
            using (var db = new BNBEntities())
            {
                var query = from b in db.PRODUTO_COSIF orderby b.COD_PRODUTO select b;
                return query.ToList();
            }
        }

        public void AddProduto(PRODUTO produto)
        {
            using (var db = new BNBEntities())
            {
                db.PRODUTOes.Add(produto);
                db.SaveChanges();
            }
        }

        public void AddProdutoCosIf(PRODUTO_COSIF produto)
        {
            using (var db = new BNBEntities())
            {
                db.PRODUTO_COSIF.Add(produto);
                db.SaveChanges();
            }
        }
    }
}
